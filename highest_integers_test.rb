require 'test/unit'
require 'test/unit/ui/console/testrunner'
require_relative 'highest_integers'

class HighestIntegersTest < Test::Unit::TestCase
  def test_all_pos_seq
    input = [1,2,3,4,5,6,7,8,9,10,11]
    expected = [8,9,10,11]
    output = highest_numbers(input)
    assert(output == expected, "Expected: #{expected} Actual: #{output}")
  end

  def test_all_pos_nonseq
    input = [1,2,3,94,5,16,7,8,229,10,11]
    expected = [11, 16, 94, 229]
    output = highest_numbers(input)
    assert(output == expected, "Expected: #{expected} Actual: #{output}")
  end

  def test_all_neg_seq
    input = [-1, -2, -3, -4, -5, -6, -7, -8, -9]
    expected = [-4,-3,-2,-1]
    output = highest_numbers(input)
    assert(output == expected, "Expected: #{expected} Actual: #{output}")
  end

  def test_all_neg_nonseq
    input = [-1, -20, -30, -4, -15, -6, -7, -8, -119]
    expected = [-7,-6,-4,-1]
    output = highest_numbers(input)
    assert(output == expected, "Expected: #{expected} Actual: #{output}")
  end

  def test_any_int
    input = [-150, -12, 0, 88, 9099, -1]
    expected = [-1,0,88,9099]
    output = highest_numbers(input)
    assert(output == expected, "Expected: #{expected} Actual: #{output}")
  end

  def test_nonunique
    input = [0,4,0,4,0,9]
    expected = [0,4,4,9]
    output = highest_numbers(input)
    assert(output == expected, "Expected: #{expected} Actual: #{output}")
  end
end
