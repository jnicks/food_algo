class Fixnum
  N_BYTES = [42].pack('i').size
  N_BITS = N_BYTES * 8
  MAX = 2 ** (N_BITS - 2) - 1
  MIN = -MAX - 1
end

def highest_numbers(input_list)
  ary = []
  4.times { ary.push(Fixnum::MIN) }
  input_list.each do |n|
    index = 0
    while index < 4 && ary[index] < n
      ary[index - 1] = ary[index] unless index == 0
      ary[index] = n
      index += 1
    end
  end
  ary
end
